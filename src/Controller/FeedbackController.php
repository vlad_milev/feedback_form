<?php

namespace App\Controller;

use App\Entity\Feedback;
use App\Events\FeedbackSendEvent;
use App\Form\Model\FeedbackFormModel;
use App\Form\FeedbackFormType;
use App\Repository\FeedbackRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FeedbackController extends AbstractController
{
    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param EventDispatcherInterface $dispatcher
     * @Route("/", name="app_form")
     */
    public function feedback(Request $request, EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $form = $this->createForm(FeedbackFormType::class, new FeedbackFormModel( $request->getClientIp() ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
             /** @var FeedbackFormModel $feedback */
            $model = $form->getData();

            $feedback= new Feedback();
            $feedback->setName( $model->name )
                ->setEmail( $model->email )
                ->setMessage( $model->message )
                ->setIp( $model->ip );

            $em->persist($feedback);
            $em->flush();

            $dispatcher->dispatch(new FeedbackSendEvent($feedback));

            $this->addFlash('flash_message', 'Your message has been success processed. Thank you!');
            return $this->redirectToRoute('app_detail', ['email' => $feedback->getEmail()]);
        }

        return $this->render('feedbacks/create.html.twig', ['feedbackForm' => $form->createView()]);
    }

    /**
     * @param string $email
     * @param FeedbackRepository $feedbackRepository
     * @Route("/feedback/{email}", name="app_detail")
     */
    public function detail($email, FeedbackRepository $feedbackRepository)
    {
        $feedbacks= $feedbackRepository->findBy(["email"=> $email], array('createdAt' => 'DESC'));

        return $this->render( "feedbacks/detail.html.twig", [
            'email' => $email,
            'feedbacks' => $feedbacks
        ]);
    }
}