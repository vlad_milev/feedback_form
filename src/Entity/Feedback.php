<?php

namespace App\Entity;

use App\Repository\FeedbackRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FeedbackRepository::class)
 */
class Feedback
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $ip;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
       return $this->id;
    }

    public function getName(): ?string
    {
       return $this->name;
    }

    public function setName(string $name): self
    {
       $this->name = $name;

       return $this;
    }

    public function getEmail(): ?string
    {
       return $this->email;
    }

    public function setEmail(string $email): self
    {
       $this->email = $email;

       return $this;
    }

    public function getMessage(): ?string
    {
       return $this->message;
    }

    public function setMessage(string $message): self
    {
       $this->message = $message;

       return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
       return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
       $this->createdAt = $createdAt;

       return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }
}
