<?php


namespace App\EventSubscriber;

use App\Events\FeedbackSendEvent;
use App\Service\Mailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FeedbackSendSubscriber implements EventSubscriberInterface
{
    /**
     * @var \App\Service\Mailer
     */
    private $mailer;

    /**
     * FeedbackSendSubscriber constructor.
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents()
    {
        return [
            FeedbackSendEvent::class => 'onFeedbackSend',
        ];
    }

    public function onFeedbackSend(FeedbackSendEvent $event)
    {
        $this->mailer->sendFeedbackMail($event->getFeedback());
    }
}