<?php


namespace App\Events;

use App\Entity\Feedback;
use Symfony\Contracts\EventDispatcher\Event;

class FeedbackSendEvent extends Event
{
    /**
     * @var Feedback
     */
    private $feedback;

    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * @return Feedback
     */
    public function getFeedback(): Feedback
    {
        return $this->feedback;
    }
}