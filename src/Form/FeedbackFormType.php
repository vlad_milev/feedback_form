<?php

namespace App\Form;

use App\Form\Model\FeedbackFormModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeedbackFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $value1 = rand(1, 30);
        $value2 = rand(1, 30);

        if (rand(0, 1) > 0)
            $value2 = -1 * $value2;

        $builder
            ->add('name',
                TextType::class,
                [
                    'label' => 'Name',
                    'required' => true,
                ]
            )
            ->add('email',
                EmailType::class,
                [
                    'label' => 'Email',
                    'required' => true,
                ]
            )
            ->add('message',
                TextareaType::class,
                [
                    'label' => 'Message',
                    'required' => true,
                    'attr' => [
                        'rows' => 3,
                    ],
                ])
            ->add('captchaValue1',
                HiddenType::class,
                [
                    'attr' => [
                        'value' => $value1
                    ]
                ])
            ->add('captchaValue2',
                HiddenType::class,
                [
                    'attr' => [
                        'value' => $value2
                    ]
                ])
            ->add('captchaResult',
                IntegerType::class,
                [
                    'label' => sprintf(
                        'Enter the result of the expression (%s %s %s) = ',
                        $value1,
                        $value2 < 0 ? '-' : '+',
                        $value2 < 0 ? -1*$value2 : $value2
                    ),
                    'required' => true,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FeedbackFormModel::class,
        ]);
    }
}
