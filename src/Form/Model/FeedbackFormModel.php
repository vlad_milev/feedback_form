<?php


namespace App\Form\Model;

use App\Validator\CaptchaCheck;
use App\Validator\EmailGuard;
use App\Validator\IPGuard;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class FeedbackFormModel
 * @package App\Form\Model
 * @CaptchaCheck
 * @EmailGuard
 * @IPGuard()
 */
class FeedbackFormModel
{
    /**
     * @Assert\NotBlank(message="Name cannot be empty")
     * @Assert\Length(max=80, maxMessage="Maximum allowed length is is 80 characters")
     */
    public $name;

    /**
     * @Assert\NotBlank(message="Email cannot be empty")
     * @Assert\Length(max=80, maxMessage="Maximum allowed length is 80 characters")
     * @Assert\Email()
     */
    public $email;

    /**
     * @Assert\NotBlank(message="Message cannot be empty")
     * @Assert\Length(max=4000, maxMessage="Maximum allowed length is 4000 characters")
     */
    public $message;

    public $captchaValue1;

    public $captchaValue2;

    public $captchaResult;

    public $ip;

    /**
     * FeedbackFormModel constructor.
     * @param string $ip
     */
    public function __construct($ip)
    {
        $this->ip = $ip;
    }
}