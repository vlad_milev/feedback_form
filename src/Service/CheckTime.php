<?php


namespace App\Service;


use App\Entity\Feedback;

class CheckTime
{
    /**
     * @var int
     */
    private $time_limitation;

    /**
     * @param int $time_limitation
     */
    public function __construct($time_limitation)
    {
        $this->time_limitation = $time_limitation;
    }

    /**
     * @param Feedback|null $feedback
     * @return bool
     */
    public function checkTimeLimitation($feedback)
    {
        if ($feedback !== null) {
            $ts_last= $feedback->getCreatedAt()->getTimestamp();
            $ts = time() - $ts_last;
            if ($ts < $this->time_limitation) {
                return true;
            }
        }
        return false;
    }
}