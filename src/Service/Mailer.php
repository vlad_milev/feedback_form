<?php

namespace App\Service;

use App\Entity\Feedback;
use App\Repository\FeedbackRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class Mailer
{
    const FROM_EMAIL = 'noreply@feedback.form';
    const FROM_NAME = 'Feedback form';

    const ADMIN_EMAIL = 'info@awardwallet.com';
    const ADMIN_NAME = 'Administrator';

    /**
     * @var \Symfony\Component\Mailer\MailerInterface
     */
    private $mailer;
    /**
     * @var \App\Repository\FeedbackRepository
     */
    private $feedbackRepository;


    /**
     * Mailer constructor.
     */
    public function __construct(MailerInterface $mailer, FeedbackRepository $feedbackRepository)
    {
        $this->mailer = $mailer;
        $this->feedbackRepository = $feedbackRepository;
    }

    /**
     * @param Address $fromAddress
     * @param Address $toAddress
     * @param string $template
     * @param string $message
     * @param array|null $context
     * @return TemplatedEmail
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    private function createTemplateEmail(Address $fromAddress, Address $toAddress, $template, $message, $context)
    {
        return $email = (new TemplatedEmail())
            ->from($fromAddress)
            ->to($toAddress)
            ->htmlTemplate($template)
            ->subject($message)
            ->context($context);
    }

    /**
     * @param TemplatedEmail $email
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    private function sendOne($email)
    {
        $this->mailer->send($email);
    }

    /**
     * @param TemplatedEmail $email
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    private function sendMany($emails)
    {
        foreach ($emails as $email) {
            $this->sendOne($email);
        }
    }

    /**
     * Отправка писем с уведомлением о поступлении нового обрещения
     *
     * @param Feedback $feedback
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendFeedbackMail(Feedback $feedback)
    {
        $email_user = $this->createTemplateEmail(
            new Address(self::FROM_EMAIL, self::FROM_NAME),
            new Address($feedback->getEmail(), $feedback->getName()),
            'email/feedback.html.twig',
            "Thanks for your message!",
            ['feedback' => $feedback]
        );

        $email_admin = $this->createTemplateEmail(
            new Address(self::FROM_EMAIL, self::FROM_NAME),
            new Address(self::ADMIN_EMAIL, self::ADMIN_NAME),
            'email/feedback_admin.html.twig',
            sprintf("New feedback from %s", $feedback->getName()),
            ['feedback' => $feedback]
        );

        $this->sendMany([$email_user, $email_admin]);
    }
}