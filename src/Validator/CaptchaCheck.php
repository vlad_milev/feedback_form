<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CaptchaCheck extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'Result of the expression {{result}} is not correct.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
