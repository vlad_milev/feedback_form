<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CaptchaCheckValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\CaptchaCheck */

        if (null === $value || '' === $value) {
            return;
        }

        if ($value->captchaResult == ($value->captchaValue1 + $value->captchaValue2)) {
            return;
        }
        // TODO: implement the validation here
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{result}}', $value->captchaResult)
            ->addViolation();
    }
}
