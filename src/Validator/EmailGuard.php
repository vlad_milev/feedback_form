<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EmailGuard extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'Слишком много запросов с вашего адреса "{{ value }}", пожалуйста подождите минуту.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
