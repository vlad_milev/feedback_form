<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IPGuard extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'Слишком много запросов с вашего компьютера с ip="{{ value }}", пожалуйста подождите минуту.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
