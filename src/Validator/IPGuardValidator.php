<?php

namespace App\Validator;

use App\Entity\Feedback;
use App\Repository\FeedbackRepository;
use App\Service\CheckTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IPGuardValidator extends ConstraintValidator
{
    /**
     * @var FeedbackRepository
     */
    private $feedbackRepository;
    /**
     * @var CheckTime
     */
    private CheckTime $checkTime;

    /**
     * IPGuardValidator constructor.
     *
     * @param FeedbackRepository $feedbackRepository
     * @param CheckTime $checkTime
     */
    public function __construct(FeedbackRepository $feedbackRepository, CheckTime $checkTime)
    {
        $this->feedbackRepository = $feedbackRepository;
        $this->checkTime = $checkTime;
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\IPGuardValidator */

        if (null === $value || '' === $value) {
            return;
        }

        /* @var $last_feedback Feedback */
        $last_feedback= $this->feedbackRepository->findOneBy(
            ["ip"=> $value->ip],
            ['createdAt' => 'DESC']
        );
        if (!$this->checkTime->checkTimeLimitation($last_feedback))
            return;

        // TODO: implement the validation here
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value->ip)
            ->addViolation();
    }
}
